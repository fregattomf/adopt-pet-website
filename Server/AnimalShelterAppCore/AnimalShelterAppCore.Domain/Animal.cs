﻿using System.Collections.Generic;

namespace AnimalShelterAppCore.Domain
{
    public enum AnimalType { Cat, Dog }
    public enum Gender { Male, Female }
    public enum IsChildFriendly { Yes, No }

    public class Animal
    {
        public Animal()
        {
            Photos = new List<AnimalPhoto>();
        }
        public int Id { get; set; }
        public AnimalType AnimalType { get; set; }
        public string Name { get; set; }
        public double Age { get; set; }
        public string Color { get; set; }
        public string Race { get; set; }
        public Gender Gender { get; set; }
        public IsChildFriendly IsChildFriendly { get; set; }
        public string Characteristics { get; set; }
        public List<AnimalPhoto> Photos { get; set; }
    }    
}

