﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalShelterAppCore.Domain
{
    public class AnimalPhoto
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int AnimalId { get; set; }
        public Animal Animal { get; set; }
    }
}
