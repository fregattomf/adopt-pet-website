﻿using System;
using Microsoft.EntityFrameworkCore;
using AnimalShelterAppCore.Domain;
using System.Collections.Generic;
using System.Text;

namespace DataLayer
{
    public class AnimalShelterContext : DbContext
    {
        public AnimalShelterContext(DbContextOptions<AnimalShelterContext> options)
            : base(options)
        {

        }

        public DbSet<Animal> Animals { get; set; }
        public DbSet<AnimalPhoto> AnimalPhotos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region AnimalSeed
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 1, Age = 2, Color = "Black", Name = "Coffee", Race = "European Shorthair", AnimalType = AnimalType.Cat, Gender = Gender.Male, IsChildFriendly = IsChildFriendly.Yes, Characteristics = "Likes to run."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 2, Age = 1, Color = "Orange", Name = "Caramel", Race = "Mixed Race", AnimalType = AnimalType.Cat, Gender = Gender.Female, IsChildFriendly = IsChildFriendly.Yes, Characteristics = "Loves to eat."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 3, Age = 0.5, Color = "Grey and White", Name = "Smoke", Race = "European Shorthair", AnimalType = AnimalType.Cat, Gender = Gender.Male, IsChildFriendly = IsChildFriendly.No, Characteristics = "Highly curious."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 4, Age = 3, Color = "Brown and White", Name = "Lady", Race = "Siamese", AnimalType = AnimalType.Cat, Gender = Gender.Female, IsChildFriendly = IsChildFriendly.No, Characteristics = "Likes to run and eat."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 5, Age = 5, Color = "Grey and White", Name = "Latte", Race = "Mixed Race", AnimalType = AnimalType.Cat, Gender = Gender.Male, IsChildFriendly = IsChildFriendly.Yes, Characteristics = "Likes to relax."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 6, Age = 2, Color = "Orange and White", Name = "Princess", Race = "Siberian", AnimalType = AnimalType.Cat, Gender = Gender.Female, IsChildFriendly = IsChildFriendly.Yes, Characteristics = "Likes to sleep and eat."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 7, Age = 3, Color = "Gray Tabby", Name = "Frasse", Race = "Mixed Race", AnimalType = AnimalType.Cat, Gender = Gender.Male, IsChildFriendly = IsChildFriendly.Yes, Characteristics = "Likes to play. Likes kids."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 8, Age = 6, Color = "Gray Tabby", Name = "Hallon", Race = "European Shorthair", AnimalType = AnimalType.Cat, Gender = Gender.Female, IsChildFriendly = IsChildFriendly.Yes, Characteristics = "Likes to run and play."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 9, Age = 2, Color = "Black and Brown", Name = "Sally", Race = "Dachshund", AnimalType = AnimalType.Dog, Gender = Gender.Female, IsChildFriendly = IsChildFriendly.Yes, Characteristics = "Likes to relax."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 10, Age = 6, Color = "Black and Brown", Name = "Luna", Race = "Mixed Race", AnimalType = AnimalType.Dog, Gender = Gender.Female, IsChildFriendly = IsChildFriendly.No, Characteristics = "Loves to go for walks."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 11, Age = 1, Color = "White", Name = "Molly", Race = "Maltese", AnimalType = AnimalType.Dog, Gender = Gender.Female, IsChildFriendly = IsChildFriendly.No, Characteristics = "Loves company."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 12, Age = 0.5, Color = "White", Name = "Rover", Race = "Mixed Race", AnimalType = AnimalType.Dog, Gender = Gender.Male, IsChildFriendly = IsChildFriendly.No, Characteristics = "Full of energy."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 13, Age = 1, Color = "White", Name = "Jupiter", Race = "Samoyed", AnimalType = AnimalType.Dog, Gender = Gender.Male, IsChildFriendly = IsChildFriendly.Yes, Characteristics = "High energy and friendly."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 14, Age = 3, Color = "Caramel, Black and White", Name = "Jojo", Race = "Corgi", AnimalType = AnimalType.Dog, Gender = Gender.Male, IsChildFriendly = IsChildFriendly.Yes, Characteristics = "Likes to chew toys and play fetch."});
            modelBuilder.Entity<Animal>().HasData(new Animal { Id = 15, Age = 5, Color = "Caramel and Black", Name = "Atlas", Race = "German Shepherd", AnimalType = AnimalType.Dog, Gender = Gender.Male, IsChildFriendly = IsChildFriendly.No, Characteristics = "Obeys many commands and is very calm."});
            #endregion

            modelBuilder.Entity<AnimalPhoto>(entity =>
            {
                entity.HasOne(d => d.Animal)
                    .WithMany(p => p.Photos)
                    .HasForeignKey("AnimalId");
            });

            #region AnimalPhotoSeed
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 1, Url = "/Images/Cats/1.jpeg", AnimalId = 1 });
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 2, Url = "/Images/Cats/2.jpeg", AnimalId = 2 });
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 3, Url = "/Images/Cats/3.jpeg", AnimalId = 3 });
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 4, Url = "/Images/Cats/4.jpeg", AnimalId = 4 });
            modelBuilder.Entity<AnimalPhoto>().HasData(
               new AnimalPhoto() { Id = 5, Url = "/Images/Cats/5.jpeg", AnimalId = 5 });
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 6, Url = "/Images/Cats/6.jpeg", AnimalId = 6 });
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 7, Url = "/Images/Cats/7.jpeg", AnimalId = 7 });
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 8, Url = "/Images/Cats/8.jpeg", AnimalId = 8 });
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 9, Url = "/Images/Dogs/1.jpeg", AnimalId = 9 });
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 10, Url = "/Images/Dogs/2.jpeg", AnimalId = 10 });            
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 11, Url = "/Images/Dogs/3.jpeg", AnimalId = 11 });            
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 12, Url = "/Images/Dogs/4.jpeg", AnimalId = 12 });            
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 13, Url = "/Images/Dogs/5.jpeg", AnimalId = 13 });           
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 14, Url = "/Images/Dogs/6.jpeg", AnimalId = 14 });            
            modelBuilder.Entity<AnimalPhoto>().HasData(
                new AnimalPhoto() { Id = 15, Url = "/Images/Dogs/7.jpeg", AnimalId = 15 });
            #endregion

        }
    }

    
}
