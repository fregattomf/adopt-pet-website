﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AnimalShelterAppCore.Data.Migrations
{
    public partial class SeedDataToPhotos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
            table: "AnimalPhotos",
            columns: new[] { "Id", "AnimalId", "PhotoAddress" },
            values: new object[,]
            {
                    { 1, 1, "/Images/Cats/1.jpeg" },
                    { 2, 2, "/Images/Cats/2.jpeg" },
                    { 3, 3, "/Images/Cats/3.jpeg" },
                    { 4, 4, "/Images/Cats/4.jpeg" },
                    { 5, 5, "/Images/Cats/5.jpeg" },
                    { 6, 6, "/Images/Cats/6.jpeg" },
                    { 7, 7, "/Images/Cats/7.jpeg" },
                    { 8, 8, "/Images/Cats/8.jpeg" },
                    { 9, 9, "/Images/Dogs/1.jpeg" },
                    { 10, 10, "/Images/Dogs/2.jpeg" }
             });
                       
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
