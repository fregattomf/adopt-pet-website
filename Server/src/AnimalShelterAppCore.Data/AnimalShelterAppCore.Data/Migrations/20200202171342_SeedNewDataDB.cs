﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace AnimalShelterAppCore.Data.Migrations
{
    public partial class SeedNewDataDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Animals",
                columns: new[] { "Id", "Age", "AnimalType", "Characteristics", "Color", "Gender", "IsChildFriendly", "Name", "Race" },
                values: new object[,]
                {
                    { 1, 2.0, 0, "Likes to run.", "Black", 0, 0, "Coffee", "European Shorthair" },
                    { 2, 1.0, 0, "Loves to eat.", "Orange", 1, 0, "Caramel", "Mixed Race" },
                    { 3, 0.5, 0, "Highly curious.", "Grey and White", 0, 1, "Smoke", "European Shorthair" },
                    { 4, 3.0, 0, "Likes to run and eat.", "Brown and White", 1, 1, "Lady", "Siamese" },
                    { 5, 5.0, 0, "Likes to relax.", "Grey and White", 0, 0, "Latte", "Mixed Race" },
                    { 6, 2.0, 0, "Likes to sleep and eat.", "Orange and White", 1, 0, "Princess", "Siberian" },
                    { 7, 3.0, 0, "Likes to play. Likes kids.", "Gray Tabby", 0, 0, "Frasse", "Mixed Race" },
                    { 8, 6.0, 0, "Likes to run and play.", "Gray Tabby", 1, 0, "Hallon", "European Shorthair" },
                    { 9, 2.0, 1, "Likes to relax.", "Black and Brown", 1, 0, "Sally", "Dachshund" },
                    { 10, 6.0, 1, "Loves to go for walks.", "Black and Brown", 1, 1, "Luna", "Mixed Race" }
                });

        }

        
    }
}
