﻿// <auto-generated />
using DataLayer;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace AnimalShelterAppCore.Data.Migrations
{
    [DbContext(typeof(AnimalShelterContext))]
    [Migration("20200202171342_SeedNewDataDB")]
    partial class SeedNewDataDB
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .HasAnnotation("ProductVersion", "3.1.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("AnimalShelterAppCore.Domain.Animal", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<double>("Age")
                        .HasColumnType("double precision");

                    b.Property<int>("AnimalType")
                        .HasColumnType("integer");

                    b.Property<string>("Characteristics")
                        .HasColumnType("text");

                    b.Property<string>("Color")
                        .HasColumnType("text");

                    b.Property<int>("Gender")
                        .HasColumnType("integer");

                    b.Property<int>("IsChildFriendly")
                        .HasColumnType("integer");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.Property<string>("Race")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Animals");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Age = 2.0,
                            AnimalType = 0,
                            Characteristics = "Likes to run.",
                            Color = "Black",
                            Gender = 0,
                            IsChildFriendly = 0,
                            Name = "Coffee",
                            Race = "European Shorthair"
                        },
                        new
                        {
                            Id = 2,
                            Age = 1.0,
                            AnimalType = 0,
                            Characteristics = "Loves to eat.",
                            Color = "Orange",
                            Gender = 1,
                            IsChildFriendly = 0,
                            Name = "Caramel",
                            Race = "Mixed Race"
                        },
                        new
                        {
                            Id = 3,
                            Age = 0.5,
                            AnimalType = 0,
                            Characteristics = "Highly curious.",
                            Color = "Grey and White",
                            Gender = 0,
                            IsChildFriendly = 1,
                            Name = "Smoke",
                            Race = "European Shorthair"
                        },
                        new
                        {
                            Id = 4,
                            Age = 3.0,
                            AnimalType = 0,
                            Characteristics = "Likes to run and eat.",
                            Color = "Brown and White",
                            Gender = 1,
                            IsChildFriendly = 1,
                            Name = "Lady",
                            Race = "Siamese"
                        },
                        new
                        {
                            Id = 5,
                            Age = 5.0,
                            AnimalType = 0,
                            Characteristics = "Likes to relax.",
                            Color = "Grey and White",
                            Gender = 0,
                            IsChildFriendly = 0,
                            Name = "Latte",
                            Race = "Mixed Race"
                        },
                        new
                        {
                            Id = 6,
                            Age = 2.0,
                            AnimalType = 0,
                            Characteristics = "Likes to sleep and eat.",
                            Color = "Orange and White",
                            Gender = 1,
                            IsChildFriendly = 0,
                            Name = "Princess",
                            Race = "Siberian"
                        },
                        new
                        {
                            Id = 7,
                            Age = 3.0,
                            AnimalType = 0,
                            Characteristics = "Likes to play. Likes kids.",
                            Color = "Gray Tabby",
                            Gender = 0,
                            IsChildFriendly = 0,
                            Name = "Frasse",
                            Race = "Mixed Race"
                        },
                        new
                        {
                            Id = 8,
                            Age = 6.0,
                            AnimalType = 0,
                            Characteristics = "Likes to run and play.",
                            Color = "Gray Tabby",
                            Gender = 1,
                            IsChildFriendly = 0,
                            Name = "Hallon",
                            Race = "European Shorthair"
                        },
                        new
                        {
                            Id = 9,
                            Age = 2.0,
                            AnimalType = 1,
                            Characteristics = "Likes to relax.",
                            Color = "Black and Brown",
                            Gender = 1,
                            IsChildFriendly = 0,
                            Name = "Sally",
                            Race = "Dachshund"
                        },
                        new
                        {
                            Id = 10,
                            Age = 6.0,
                            AnimalType = 1,
                            Characteristics = "Loves to go for walks.",
                            Color = "Black and Brown",
                            Gender = 1,
                            IsChildFriendly = 1,
                            Name = "Luna",
                            Race = "Mixed Race"
                        });
                });

            modelBuilder.Entity("AnimalShelterAppCore.Domain.AnimalPhoto", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int>("AnimalId")
                        .HasColumnType("integer");

                    b.Property<string>("PhotoAddress")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("AnimalId");

                    b.ToTable("AnimalPhotos");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AnimalId = 1,
                            PhotoAddress = "/Images/Cats/1.jpeg"
                        },
                        new
                        {
                            Id = 2,
                            AnimalId = 2,
                            PhotoAddress = "/Images/Cats/2.jpeg"
                        },
                        new
                        {
                            Id = 3,
                            AnimalId = 3,
                            PhotoAddress = "/Images/Cats/3.jpeg"
                        },
                        new
                        {
                            Id = 4,
                            AnimalId = 4,
                            PhotoAddress = "/Images/Cats/4.jpeg"
                        },
                        new
                        {
                            Id = 5,
                            AnimalId = 5,
                            PhotoAddress = "/Images/Cats/5.jpeg"
                        },
                        new
                        {
                            Id = 6,
                            AnimalId = 6,
                            PhotoAddress = "/Images/Cats/6.jpeg"
                        },
                        new
                        {
                            Id = 7,
                            AnimalId = 7,
                            PhotoAddress = "/Images/Cats/7.jpeg"
                        },
                        new
                        {
                            Id = 8,
                            AnimalId = 8,
                            PhotoAddress = "/Images/Cats/8.jpeg"
                        },
                        new
                        {
                            Id = 9,
                            AnimalId = 9,
                            PhotoAddress = "/Images/Dogs/1.jpeg"
                        },
                        new
                        {
                            Id = 10,
                            AnimalId = 10,
                            PhotoAddress = "/Images/Dogs/2.jpeg"
                        });
                });

            modelBuilder.Entity("AnimalShelterAppCore.Domain.AnimalPhoto", b =>
                {
                    b.HasOne("AnimalShelterAppCore.Domain.Animal", "Animal")
                        .WithMany("Photos")
                        .HasForeignKey("AnimalId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
