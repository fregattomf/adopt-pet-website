﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AnimalShelterAppCore.Data.Migrations
{
    public partial class rename_photoAddress_to_Url : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhotoAddress",
                table: "AnimalPhotos");

            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "AnimalPhotos",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 1,
                column: "Url",
                value: "/Images/Cats/1.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 2,
                column: "Url",
                value: "/Images/Cats/2.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 3,
                column: "Url",
                value: "/Images/Cats/3.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 4,
                column: "Url",
                value: "/Images/Cats/4.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 5,
                column: "Url",
                value: "/Images/Cats/5.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 6,
                column: "Url",
                value: "/Images/Cats/6.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 7,
                column: "Url",
                value: "/Images/Cats/7.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 8,
                column: "Url",
                value: "/Images/Cats/8.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 9,
                column: "Url",
                value: "/Images/Dogs/1.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 10,
                column: "Url",
                value: "/Images/Dogs/2.jpeg");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Url",
                table: "AnimalPhotos");

            migrationBuilder.AddColumn<string>(
                name: "PhotoAddress",
                table: "AnimalPhotos",
                type: "text",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 1,
                column: "PhotoAddress",
                value: "/Images/Cats/1.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 2,
                column: "PhotoAddress",
                value: "/Images/Cats/2.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 3,
                column: "PhotoAddress",
                value: "/Images/Cats/3.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 4,
                column: "PhotoAddress",
                value: "/Images/Cats/4.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 5,
                column: "PhotoAddress",
                value: "/Images/Cats/5.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 6,
                column: "PhotoAddress",
                value: "/Images/Cats/6.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 7,
                column: "PhotoAddress",
                value: "/Images/Cats/7.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 8,
                column: "PhotoAddress",
                value: "/Images/Cats/8.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 9,
                column: "PhotoAddress",
                value: "/Images/Dogs/1.jpeg");

            migrationBuilder.UpdateData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 10,
                column: "PhotoAddress",
                value: "/Images/Dogs/2.jpeg");
        }
    }
}
