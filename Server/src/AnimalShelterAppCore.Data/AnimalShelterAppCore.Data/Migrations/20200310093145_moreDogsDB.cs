﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AnimalShelterAppCore.Data.Migrations
{
    public partial class moreDogsDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Animals",
                columns: new[] { "Id", "Age", "AnimalType", "Characteristics", "Color", "Gender", "IsChildFriendly", "Name", "Race" },
                values: new object[,]
                {
                    { 11, 1.0, 1, "Loves company.", "White", 1, 1, "Molly", "Maltese" },
                    { 12, 0.5, 1, "Full of energy.", "White", 0, 1, "Rover", "Mixed Race" },
                    { 13, 1.0, 1, "High energy and friendly.", "White", 0, 0, "Jupiter", "Samoyed" },
                    { 14, 3.0, 1, "Likes to chew toys and play fetch.", "Caramel, Black and White", 0, 0, "Jojo", "Corgi" },
                    { 15, 5.0, 1, "Obeys many commands and is very calm.", "Caramel and Black", 0, 1, "Atlas", "German Shepherd" }
                });

            migrationBuilder.InsertData(
                table: "AnimalPhotos",
                columns: new[] { "Id", "AnimalId", "Url" },
                values: new object[,]
                {
                    { 11, 11, "/Images/Dogs/3.jpeg" },
                    { 12, 12, "/Images/Dogs/4.jpeg" },
                    { 13, 13, "/Images/Dogs/5.jpeg" },
                    { 14, 14, "/Images/Dogs/6.jpeg" },
                    { 15, 15, "/Images/Dogs/7.jpeg" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "AnimalPhotos",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Animals",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Animals",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Animals",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Animals",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Animals",
                keyColumn: "Id",
                keyValue: 15);
        }
    }
}
