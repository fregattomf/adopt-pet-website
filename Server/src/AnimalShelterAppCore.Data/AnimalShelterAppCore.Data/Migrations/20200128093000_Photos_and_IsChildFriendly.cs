﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AnimalShelterAppCore.Data.Migrations
{
    public partial class Photos_and_IsChildFriendly : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Photo",
                table: "AnimalPhotos");

            migrationBuilder.AlterColumn<double>(
                name: "Age",
                table: "Animals",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<int>(
                name: "IsChildFriendly",
                table: "Animals",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PhotoAddress",
                table: "AnimalPhotos",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsChildFriendly",
                table: "Animals");

            migrationBuilder.DropColumn(
                name: "PhotoAddress",
                table: "AnimalPhotos");

            migrationBuilder.AlterColumn<int>(
                name: "Age",
                table: "Animals",
                type: "integer",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AddColumn<byte[]>(
                name: "Photo",
                table: "AnimalPhotos",
                type: "bytea",
                nullable: true);
        }
    }
}
