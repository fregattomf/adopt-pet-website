using AnimalShelterAppCore.Domain;
using DataLayer;
using DataAccess;
using Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using Xunit;
using AutoMapper;
using FluentAssertions;

namespace XUnitTestProject1
{
    public class IntegrationTests
    {

        [Fact]
        public void Should_GetAnimal_When_ValidID()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<AnimalShelterContext>()
                .UseInMemoryDatabase(databaseName: "GetAnimal")
                .Options;
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DomainModelsProfile());
            });
            var mapper = mockMapper.CreateMapper();

            //Act
            // Run the test against one instance of the context
            var animalSaved = new AnimalDTO { 
                Id = 1,
                Age = 2, 
                Color = "Black", 
                Name = "Coffee", 
                Race = "European Shorthair", 
                AnimalType = AnimalTypeDTO.Cat, 
                Gender = GenderDTO.Male, 
                IsChildFriendly = IsChildFriendlyDTO.Yes, 
                Characteristics = "Likes to run." 
            };
            using (var context = new AnimalShelterContext(options))
            {
                var service = new AnimalDataAccess(context, mapper);
                service.AddAnimal(animalSaved);
            }
            //Assert
            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = new AnimalShelterContext(options))
            {
                var bizlogic = new AnimalDataAccess(context, mapper);
                var animalRetrieved = bizlogic.GetAnimalById(animalSaved.Id);
                animalRetrieved.Id.Should().Be(animalSaved.Id);
            }
        }

        [Fact]
        public void Should_ReturnNull_When_InvalidID()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<AnimalShelterContext>()
                .UseInMemoryDatabase(databaseName: "GetAnimalWithID")
                .Options;
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DomainModelsProfile());
            });
            var mapper = mockMapper.CreateMapper();

            //Act
            // Run the test against one instance of the context
            var animalSaved = new AnimalDTO
            {
                Id = 1,
                Age = 2,
                Color = "Black",
                Name = "Coffee",
                Race = "European Shorthair",
                AnimalType = AnimalTypeDTO.Cat,
                Gender = GenderDTO.Male,
                IsChildFriendly = IsChildFriendlyDTO.Yes,
                Characteristics = "Likes to run."
            };
            using (var context = new AnimalShelterContext(options))
            {
                var service = new AnimalDataAccess(context, mapper);
                service.AddAnimal(animalSaved);
            }
            //Assert
            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = new AnimalShelterContext(options))
            {
                var bizlogic = new AnimalDataAccess(context, mapper);
                var animalRetrieved = bizlogic.GetAnimalById(2);
                animalRetrieved.Should().Be(null);
            }
        }

        [Fact]
        public void Should_DeleteAnimal_When_ValidId()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<AnimalShelterContext>()
                .UseInMemoryDatabase(databaseName: "DeleteAnimal")
                .Options;
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DomainModelsProfile());
            });
            var mapper = mockMapper.CreateMapper();

            //Act
            var animalSaved = new AnimalDTO
            {
                Id = 1,
                Age = 2,
                Color = "Black",
                Name = "Coffee",
                Race = "European Shorthair",
                AnimalType = AnimalTypeDTO.Cat,
                Gender = GenderDTO.Male,
                IsChildFriendly = IsChildFriendlyDTO.Yes,
                Characteristics = "Likes to run."
            };
            using (var context = new AnimalShelterContext(options))
            {
                var service = new AnimalDataAccess(context, mapper);
                service.AddAnimal(animalSaved);
            }

            //Assert
            using (var context = new AnimalShelterContext(options))
            {
                var bizlogic = new AnimalDataAccess(context, mapper);
                bizlogic.DeleteAnimalById(animalSaved.Id);
                var animalRetrieved = bizlogic.GetAnimalById(animalSaved.Id);
                animalRetrieved.Should().Be(null);
            }
        }

        //[Fact]
        //public void BizDataAddZeroAnimalsReturnsZeroAddedAnimals()
        //{
        //    var options = new DbContextOptionsBuilder<AnimalShelterContext>()
        //        .UseInMemoryDatabase(databaseName: "AddAnimal")
        //        .Options;

        //    int totAnimals = 0;

        //    using (var context = new AnimalShelterContext(options))
        //    {
        //        var service = new BusinessLogicData(context);
        //        for (int i = 1; i <= totAnimals; i++)
        //        {
        //            var animal = new Animal();
        //            service.AddAnimal(animal);
        //            context.SaveChanges();
        //        }
        //    }

        //    // Use a separate instance of the context to verify correct data was saved to database
        //    using (var context = new AnimalShelterContext(options))
        //    {
        //        var bizlogic = new BusinessLogicData(context);
        //        Assert.Equal(totAnimals, bizlogic.GetAllAnimals().Count());
        //    }
        //}

        //[Fact]
        //public void BizDataAddTenAnimalsReturnsTenAddedAnimals()
        //{
        //    var options = new DbContextOptionsBuilder<AnimalShelterContext>()
        //        .UseInMemoryDatabase(databaseName: "AddAnimal")
        //        .Options;

        //    int totAnimals = 10;

        //    using (var context = new AnimalShelterContext(options))
        //    {
        //        var service = new BusinessLogicData(context);
        //        for (int i = 1; i <= totAnimals; i++)
        //        {
        //            var animal = new Animal();
        //            service.AddAnimal(animal);
        //            context.SaveChanges();
        //        }
        //    }

        //    using (var context = new AnimalShelterContext(options))
        //    {
        //        var bizlogic = new BusinessLogicData(context);
        //        Assert.Equal(totAnimals, bizlogic.GetAllAnimals().Count());
        //    }
        //}

        //[Fact]
        //public void BixDataGetAllCatsReturnsAllCats()
        //{
        //    var options = new DbContextOptionsBuilder<AnimalShelterContext>()
        //        .UseInMemoryDatabase(databaseName: "AddCatsDogs")
        //        .Options;

        //    int totCats = 10;
        //    int totDogs = 5;

        //    using (var context = new AnimalShelterContext(options))
        //    {
        //        var service = new BusinessLogicData(context);
        //        for (int i = 1; i <= totCats; i++)
        //        {
        //            var animal = new Animal { AnimalType = AnimalType.Cat };
        //            service.AddAnimal(animal);
        //            context.SaveChanges();
        //        }
        //    }
        //    using (var context = new AnimalShelterContext(options))
        //    {
        //        var service = new BusinessLogicData(context);
        //        for (int i = 1; i <= totDogs; i++)
        //        {
        //            var animal = new Animal { AnimalType = AnimalType.Dog };
        //            service.AddAnimal(animal);
        //            context.SaveChanges();
        //        }
        //    }

        //    using (var context = new AnimalShelterContext(options))
        //    {
        //        var bizlogic = new BusinessLogicData(context);
        //        var retrievedAnimals = bizlogic.GetAllCats();
        //        foreach (var animals in retrievedAnimals)
        //        {
        //            Assert.Equal(AnimalType.Cat, animals.AnimalType);
        //        }
        //    }
        //}

        //[Fact]
        //public void BixDataGetAllDogsReturnsAllDogs()
        //{
        //    var options = new DbContextOptionsBuilder<AnimalShelterContext>()
        //        .UseInMemoryDatabase(databaseName: "AddCatsDogs")
        //        .Options;

        //    int totCats = 10;
        //    int totDogs = 5;

        //    using (var context = new AnimalShelterContext(options))
        //    {
        //        var service = new BusinessLogicData(context);
        //        for (int i = 1; i <= totCats; i++)
        //        {
        //            var animal = new Animal { AnimalType = AnimalType.Cat };
        //            service.AddAnimal(animal);
        //            context.SaveChanges();
        //        }
        //    }
        //    using (var context = new AnimalShelterContext(options))
        //    {
        //        var service = new BusinessLogicData(context);
        //        for (int i = 1; i <= totDogs; i++)
        //        {
        //            var animal = new Animal { AnimalType = AnimalType.Dog };
        //            service.AddAnimal(animal);
        //            context.SaveChanges();
        //        }
        //    }

        //    using (var context = new AnimalShelterContext(options))
        //    {
        //        var bizlogic = new BusinessLogicData(context);
        //        var retrievedAnimals = bizlogic.GetAllDogs();
        //        foreach (var animals in retrievedAnimals)
        //        {
        //            Assert.Equal(AnimalType.Dog, animals.AnimalType);
        //        }
        //    }
        //}
    }
}
