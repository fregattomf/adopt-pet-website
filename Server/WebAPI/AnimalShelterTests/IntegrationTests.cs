﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnimalShelterAppCore.Data;
using AnimalShelterAppCore.Domain;
using WebAPI;
using System.Linq;

namespace AnimalShelterTests
{
    [TestClass]
    class IntegrationTests
    {
        [TestMethod]
        public void BizDataGetAnimalReturnsAnimal()
        {
            //Arrange (set up builder & seed data)
            var builder = new DbContextOptionsBuilder<AnimalShelterContext>();
            builder.UseInMemoryDatabase("GetAnimal");
            int animalId = SeedWithOneAnimal(builder.Options);



            //Act (call the method)
            using (var context = new AnimalShelterContext(builder.Options))
            {
                var bizlogic = new BusinessLogicData(context);
                var animalRetrieved = bizlogic.GetAnimalById(animalId);
                //Assert (check the results)
                Assert.AreEqual(animalId, animalRetrieved.Id);
            };

        }

        private int SeedWithOneAnimal(DbContextOptions<AnimalShelterContext> options)
        {
            using (var seedcontext = new AnimalShelterContext(options))
            {
                var animal = new Animal();
                seedcontext.Animals.Add(animal);
                seedcontext.SaveChanges();
                return animal.Id;
            }
        }

    }
}
