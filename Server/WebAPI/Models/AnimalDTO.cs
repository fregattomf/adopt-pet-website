﻿using System;
using System.Collections.Generic;

namespace Models
{
    public enum AnimalTypeDTO { Cat, Dog }
    public enum GenderDTO { Male, Female }
    public enum IsChildFriendlyDTO { Yes, No }
    public class AnimalDTO
    {
        public AnimalDTO()
        {
            Photos = new List<AnimalPhotoDTO>();
        }
        public int Id { get; set; }
        public AnimalTypeDTO AnimalType { get; set; }
        public string Name { get; set; }
        public double Age { get; set; }
        public string Color { get; set; }
        public string Race { get; set; }
        public GenderDTO Gender { get; set; }
        public IsChildFriendlyDTO IsChildFriendly { get; set; }
        public string Characteristics { get; set; }
        public List<AnimalPhotoDTO> Photos { get; set; }

    }
}
