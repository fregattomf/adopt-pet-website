﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class AnimalPhotoDTO
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int AnimalId { get; set; }
        public AnimalDTO Animal { get; set; }
    }
}
