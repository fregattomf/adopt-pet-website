﻿using DataAccess;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AnimalShelterAppCore.Domain;


namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalController : ControllerBase
    {
        private readonly IAnimalData _animalData;
        public AnimalController(IAnimalData animalData)
        {
            _animalData = animalData;
        }

        // GET: api/Animal
        [HttpGet]
        public ActionResult<AnimalDTO> Get()
        {   
            var data = _animalData.GetAllAnimals();
            return Ok(data);
        }        
        
        // GET: localhost:5001/cats
        [HttpGet("/cats")]
        public ActionResult<AnimalDTO> GetCats()
        {   

            var data = _animalData.GetAllCats();
            if (data == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(data);
            }
        }

        // GET: https://localhost:5001/api/animal/find/6
        [HttpGet("find/{id}", Name = "GetAnimalByID")]
        public ActionResult<AnimalDTO> GetAnimalByID(int id)
        {

            var data = _animalData.GetAnimalById(id);
            if (data == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(data);
            }
        }

        // GET: localhost:5001/dogs
        [HttpGet("/dogs")]
        public ActionResult<AnimalDTO> GetDogs()
        {   

            var data = _animalData.GetAllDogs();
            if (data == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(data);
            }
        }

        // POST: api/Animal
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Animal/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
