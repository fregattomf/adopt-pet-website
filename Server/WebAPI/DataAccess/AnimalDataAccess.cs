﻿using AnimalShelterAppCore.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using DataLayer;
using Models;
using AutoMapper;


// TODO: implement delete AnimalPhoto
// TODO: implement update AnimalPhoto

namespace DataAccess
{
    public class AnimalDataAccess : IAnimalData
    {
        private readonly AnimalShelterContext _context;
        private readonly IMapper _mapper;
        public AnimalDataAccess(AnimalShelterContext db, IMapper mapper)
        {
            this._context = db;
            _mapper = mapper;
        }
        public int Commit()
        {
            // Nothing changes in db until Commit is called           
            // Returns number of rows affected in db:
            return _context.SaveChanges();
        }
        public AnimalDTO AddAnimal(AnimalDTO newAnimalDTO)
        {
            var dbAnimal = _mapper.Map<Animal>(newAnimalDTO);
            _context.Animals.Add(dbAnimal);
            _context.SaveChanges();
            return newAnimalDTO;
        }

        public AnimalDTO GetAnimalById(int id)
        {
            var query = _context.Animals;
            var dbAnimal = query
                .Include(a => a.Photos)
                .SingleOrDefault(x => x.Id == id);
            var animalDTO = _mapper.Map<AnimalDTO>(dbAnimal);
            return animalDTO;
        }

        public void DeleteAnimalById(int id)
        {
            var dbAnimal = _context.Animals.Find(id);
            _context.Animals.Remove(dbAnimal);
            _context.SaveChanges();
        }

        public IEnumerable<AnimalDTO> GetAllAnimals()
        {
            // Eager loading:
            var query = _context.Animals;
            var dbAnimals = query
                .Include(a => a.Photos)
                .ToList();
            var mappedAnimals = _mapper.Map<IEnumerable<AnimalDTO>>(dbAnimals);

            return mappedAnimals;
        }        
        
        public IEnumerable<AnimalDTO> GetAllCats()
        {
            var query = _context.Animals;
            var dbCats = query
                .Where(a => a.AnimalType == AnimalType.Cat)
                .Include(a => a.Photos)
                .ToList();
            var mappedCats = _mapper.Map<IEnumerable<AnimalDTO>>(dbCats);
            return mappedCats;
        }

        public IEnumerable<AnimalDTO> GetAllDogs()
        {
            var query = _context.Animals;
            var dbDogs = query
                .Where(a => a.AnimalType == AnimalType.Dog)
                .Include(a => a.Photos)
                .ToList();
            var mappedDogs = _mapper.Map<IEnumerable<AnimalDTO>>(dbDogs);
            return mappedDogs;
        }

        public AnimalDTO Update(AnimalDTO updatedAnimal)
        {
            // Attach helps to track changes about desired objects
            var dbAnimal = _mapper.Map<Animal>(updatedAnimal);
            var entity = _context.Animals.Attach(dbAnimal);
            entity.State = EntityState.Modified;
            return updatedAnimal;
        }
    }
}
