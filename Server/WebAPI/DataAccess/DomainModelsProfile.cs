﻿using System;
using System.Collections.Generic;
using System.Text;
using AnimalShelterAppCore.Domain;
using AutoMapper;

namespace DataAccess
{
    // AutoMapper is an object-object mapper.
    // More info:
    //https://automapper.readthedocs.io/en/latest/Configuration.html
    public class DomainModelsProfile : Profile
    {
        public DomainModelsProfile()
        {
            CreateMap<Animal, Models.AnimalDTO>().ReverseMap();

            CreateMap<AnimalPhoto, Models.AnimalPhotoDTO>().ReverseMap();
        }
    }
}
