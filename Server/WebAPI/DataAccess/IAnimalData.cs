﻿using AnimalShelterAppCore.Domain;
using DataLayer;
using Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

// TODO: implement delete AnimalPhoto
// TODO: implement update AnimalPhoto

namespace DataAccess
{
    public interface IAnimalData
    {
        IEnumerable<AnimalDTO> GetAllAnimals();
        //IEnumerable<Animal> GetAllAnimals();
        IEnumerable<AnimalDTO> GetAllCats();
        IEnumerable<AnimalDTO> GetAllDogs();
        AnimalDTO GetAnimalById(int id);
        AnimalDTO Update(AnimalDTO updatedAnimal);
        int Commit();
        AnimalDTO AddAnimal(AnimalDTO newAnimal);
        void DeleteAnimalById(int id);

    }
   
}
