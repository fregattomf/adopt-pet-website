import React, { Component } from "react";
import { Router, Switch, Route } from "react-router-dom";

import Home from "./components/Home";
import About from "./components/About";
import Cats from "./components/Cats";
import Dogs from "./components/Dogs";
import Login from "./components/Login";
import AnimalDetail from "./components/AnimalDetail";
import history from "./history";

export default class Routes extends Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/About" component={About} />
          <Route path="/Cats" component={Cats} />
          <Route path="/Dogs" component={Dogs} />
          <Route path="/Login" component={Login} />
          <Route path="/AnimalDetail" component={AnimalDetail} />
        </Switch>
      </Router>
    );
  }
}
