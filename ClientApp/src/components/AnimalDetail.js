import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";

//import Petcard from "./Petcard";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(3),
    width: "100%",
    maxWidth: 450,
  },
}));

function isFemale(param) {
  if (param === 0) {
    return "Male";
  } else {
    return "Female";
  }
}

function AnimalDetail() {
  const classes = useStyles();

  let [animal, setPets] = useState([]);
  useEffect(() => {
    console.log("loaded");
    // Put axios here with a call to your server
    axios.get("https://localhost:5001/api/animal/find/6").then((res) => {
      setPets(res.data);
    });
  }, []);

  // var catComponents = animal.map((p, i) => {
  //   return (
  //     <Grid item md={3} key={i}>
  //       <Petcard
  //         petimage={p.photos[0].url}
  //         petname={p.name}
  //         petinfo={p.age + " years old " + isFemale(p.gender) + " " + p.race}
  //       />
  //     </Grid>
  //   );
  // });

  return (
    <div>
      <p>Meet {animal.name}:</p>
      <div className={classes.root}>
        <Paper elevation={3} alignitems="flex-start">
          {/* <img src={animal.photos[0].url} style={{ width: 650 }} alt="pet" /> */}
          <img src={"/Images/Cats/6.jpeg"} style={{ width: 450 }} alt="pet" />
          <List className={classes.root}>
            <ListItem>{"Age: " + animal.age}</ListItem>
            <Divider />
            <ListItem>{"Gender: " + isFemale(animal.gender)}</ListItem>
            <Divider />
            <ListItem>{"Color: " + animal.color}</ListItem>
            <Divider />
            <ListItem>{"Race: " + animal.race}</ListItem>
            <Divider />
            <ListItem>{"Personality: " + animal.characteristics}</ListItem>
          </List>
        </Paper>
      </div>
    </div>
  );
}

export default AnimalDetail;
