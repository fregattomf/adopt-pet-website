import React from "react";

function HomepageImage() {
  return (
    <img
      src={"/Images/Cats/cover.jpeg"}
      style={{ width: 650 }}
      alt="Happy cats"
    />
  );
}

export default HomepageImage;
