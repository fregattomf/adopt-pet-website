import React, { useEffect, useState } from "react";
import Petcard from "./Petcard";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import axios from "axios";

//TODO: searchbar

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

function isFemale(param) {
  if (param === 0) {
    return "Male";
  } else {
    return "Female";
  }
}

function Home() {
  const classes = useStyles();

  let [animals, setPets] = useState([]);
  useEffect(() => {
    console.log("loaded");
    // Put axios here with a call to your server
    axios.get("https://localhost:5001/api/Animal").then((res) => {
      setPets(res.data);
    });
  }, []);

  var animalComponents = animals.map((p, i) => {
    return (
      <Grid item md={3} key={i}>
        <Petcard
          petimage={p.photos[0].url}
          petname={p.name}
          petid={p.id}
          petinfo={
            p.age + " years old - " + isFemale(p.gender) + " - " + p.race
          }
        />
      </Grid>
    );
  });

  return (
    <div>
      <p>Your new best friend might be here:</p>
      <div className={classes.root}>
        <Grid
          container
          spacing={3}
          direction="row"
          justify="center"
          alignitems="flex-start"
        >
          {animalComponents}
        </Grid>
      </div>
    </div>
  );
}

export default Home;
