import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
//import AnimalDetail from "./AnimalDetail";

//import { Router, Route } from "react-router-dom";

const useStyles = makeStyles({
  card: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
});

function goToPetDetails(props) {
  console.log(`hello, ${props.petname}`);
  console.log(`your id is:, ${props.petid}`);

  // return (
  //   <Router>
  //     <Route path="/animalDetail/:${props.petid}" component={AnimalDetail} />
  //   </Router>
  // );
}

function Petcard(props) {
  const classes = useStyles();
  const cardimage = props.petimage;

  return (
    <Card className={classes.card}>
      <CardActionArea onClick={() => goToPetDetails(props)}>
        <CardMedia
          className={classes.media}
          image={cardimage}
          title="Pet Image"
        />
        <CardContent>
          <Typography
            className="animal-name"
            gutterBottom
            variant="h5"
            component="h2"
          >
            {props.petname}
          </Typography>
          <Typography
            className="animal-info"
            variant="h6"
            color="textSecondary"
            component="p"
          >
            {props.petinfo}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button
          size="small"
          color="primary"
          onClick={() => goToPetDetails(props)}
        >
          Learn More
        </Button>
      </CardActions>
    </Card>
  );
}

export default Petcard;
