import React from "react";
import HomepageImage from "./HomepageImage";

function About() {
  return (
    <div>
      <HomepageImage />
      <p>Animal Shelter Adoption Webpage</p>
      <a
        className="App-link"
        href="https://gitlab.com/fregattomf"
        target="_blank"
        rel="noopener noreferrer"
      >
        Made by Marcela Fregatto
      </a>
    </div>
  );
}

export default About;
