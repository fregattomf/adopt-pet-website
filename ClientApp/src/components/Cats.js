import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Petcard from "./Petcard";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

function isFemale(param) {
  if (param === 0) {
    return "Male";
  } else {
    return "Female";
  }
}

function Cats() {
  const classes = useStyles();

  let [cats, setPets] = useState([]);
  useEffect(() => {
    console.log("loaded");
    // Put axios here with a call to your server
    axios.get("https://localhost:5001/cats").then((res) => {
      setPets(res.data);
    });
  }, []);

  var catComponents = cats.map((p, i) => {
    return (
      <Grid item md={3} key={i}>
        <Petcard
          petimage={p.photos[0].url}
          petname={p.name}
          petinfo={
            p.age + " years old - " + isFemale(p.gender) + " - " + p.race
          }
        />
      </Grid>
    );
  });

  return (
    <div>
      <p>Meet our cats:</p>
      <div className={classes.root}>
        <Grid
          container
          spacing={3}
          direction="row"
          justify="center"
          alignitems="flex-start"
        >
          {catComponents}
        </Grid>
      </div>
    </div>
  );
}

export default Cats;
