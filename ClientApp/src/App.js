import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Home from "./components/Home";
import About from "./components/About";
import Cats from "./components/Cats";
import Dogs from "./components/Dogs";
import Login from "./components/Login";
import AnimalDetail from "./components/AnimalDetail";
import "./App.css";

export default function App() {
  console.log(process.env.PUBLIC_URL);

  return (
    <div className="App">
      <Router>
        <nav className="navbar navbar-inverse">
          <div className="container-fluid">
            <div className="navbar-header">
              <div className="navbar-brand">
                <Link to="/about"> React Animal Shelter</Link>
              </div>
            </div>
            <ul className="nav navbar-nav">
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/cats">Cats</Link>
              </li>
              <li>
                <Link to="/dogs">Dogs</Link>
              </li>
            </ul>
            <ul className="nav navbar-nav navbar-right">
              <li>
                <Link to="/login">
                  <span className="glyphicon glyphicon-log-in"></span> Login
                </Link>
              </li>
            </ul>
            <div className="App-content">
              <Switch>
                <Route exact path="/">
                  <Home />
                </Route>
                <Route path="/about">
                  <About />
                </Route>
                <Route path="/cats">
                  <Cats />
                </Route>
                <Route path="/dogs">
                  <Dogs />
                </Route>
                <Route path="/login">
                  <Login />
                </Route>
                <Route path="/animalDetail">
                  <AnimalDetail />
                </Route>
              </Switch>
            </div>
          </div>
        </nav>
      </Router>
    </div>
  );
}
