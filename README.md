# adopt-pet-website

This project is to learn React, ASP.NET core, Entity Framework Core, Code-First Database

Current status: work-in-progress

Printscreens of the webpage:


Home page:

![Home Page](https://gitlab.com/fregattomf/adopt-pet-website/-/raw/master/Screenshots_of_the_webpage/home_page.PNG)

Cats page:

![Catpage](https://gitlab.com/fregattomf/adopt-pet-website/-/raw/master/Screenshots_of_the_webpage/cats_page.PNG)

About page:

![aboutpage](https://gitlab.com/fregattomf/adopt-pet-website/-/raw/master/Screenshots_of_the_webpage/about_page.PNG)

Dogs page on mobile view:

![Dogpagemobile](https://gitlab.com/fregattomf/adopt-pet-website/-/raw/master/Screenshots_of_the_webpage/dogs_mobile.PNG)




-------------------


First time setup after cloning:

npm install

npm install axios

npm install env-cmd


Install material design for react:

npm install @material-ui/core

Install Bootstrap:

npm install bootstrap@3

---------------------

Add first DB Migration using Visual Studio Package Manager Console:

add-migration MyFirstMigration


---------------------

To update the database run in .Data project's PM:

Update-Database

---------------------

